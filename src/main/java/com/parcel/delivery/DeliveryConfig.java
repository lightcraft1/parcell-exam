package com.parcel.delivery;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.parcel.delivery.model.ParcelRules;
import io.github.cdimascio.dotenv.Dotenv;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;

@Configuration
public class DeliveryConfig {
    public static final Dotenv dotenv = Dotenv.load();

    //TODO: refactor env variables is fetched in springboot standard

    public static final String PARCEL_RULE_CONFIG_FILE =
            dotenv.get("PARCEL_RULE_CONFIG_FILE") !=null ?
            dotenv.get("PARCEL_RULE_CONFIG_FILE") : "parcel-rule-config.json";

    public static final boolean PARCEL_ACCEPT_DISCOUNT =
            dotenv.get("PARCEL_ACCEPT_DISCOUNT") == null ||
            Boolean.parseBoolean(dotenv.get("PARCEL_ACCEPT_DISCOUNT"));

    public static final String MOCKLAB_BASE_URL =
        dotenv.get("MOCKLAB_BASE_URL") != null ?
        dotenv.get("MOCKLAB_BASE_URL") : "https//mynt-exam.mocklab.io";

    public static final String MOCKLAB_API_KEY =
            dotenv.get("MOCKLAB_API_KEY") != null ?
                    dotenv.get("MOCKLAB_API_KEY") : "";


    //serialize the parcel rule
    @Bean
    public ParcelRules getParcelRules() throws IOException {
        // TODO: can be pulled from a db or s3 config
        Resource resource = new ClassPathResource(DeliveryConfig.PARCEL_RULE_CONFIG_FILE);
        byte[] bytes = FileCopyUtils.copyToByteArray(resource.getInputStream());
        String json = new String(bytes, StandardCharsets.UTF_8);

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, ParcelRules.class);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        //TODO: add this to env variable or application.properties
        return builder
                .setConnectTimeout(Duration.ofMillis(5000))
                .setReadTimeout(Duration.ofMillis(5000))
                .build();
    }

    public enum ComparisonOperator {
        GREATER_THAN(">"),
        LESS_THAN("<"),
        EQUALS("="),
        GREATER_THAN_OR_EQUAL_TO(">="),
        LESS_THAN_OR_EQUAL_TO("<="),
        NONE("none");

        private final String symbol;

        ComparisonOperator(String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol() {
            return this.symbol;
        }

        public static ComparisonOperator fromSymbol(String symbol) {
            for (ComparisonOperator operator : values()) {
                if (operator.getSymbol().equals(symbol)) {
                    return operator;
                }
            }
            throw new IllegalArgumentException("Invalid symbol: " + symbol);
        }
    }

    public enum ParcelCategory {
        REJECT,
        HEAVY,
        SMALL,
        MEDIUM,
        LARGE
    }
}
