package com.parcel.delivery;

import com.parcel.delivery.model.DeliveryCostResponse;
import com.parcel.delivery.model.ParcelRequest;
import com.parcel.delivery.model.Voucher;
import com.parcel.delivery.service.DeliveryService;
import com.parcel.delivery.service.VoucherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/delivery")
public class DeliveryController {
    //TODO: exception handling
    //TODO: fix env variables e.g. System.setProperty("mocklab.baseUrl", dotenv.get("MOCKLAB_BASE_URL"));
    //TODO: security
    //TODO: test
    private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryController.class);
    // other fields...
    private final DeliveryService deliveryService;
    private final VoucherService voucherService;

    @Autowired
    public DeliveryController(DeliveryService deliveryService, VoucherService voucherService) {
        this.deliveryService = deliveryService;
        this.voucherService = voucherService;
    }

    //@PostMapping("/calculate")
    //previous implementation without completable future
    public ResponseEntity<DeliveryCostResponse>
    computeDelivery(@Valid @RequestBody ParcelRequest request) throws IOException {
        DeliveryCostResponse response = deliveryService.calculate(request);

        if (DeliveryConfig.PARCEL_ACCEPT_DISCOUNT &&
                !request.getVoucherCode().isEmpty() &&
                request.getVoucherCode() != null) {
            try {
                Voucher voucher = voucherService.callVoucherAPI(request.getVoucherCode());
                response.setDiscount(voucher.getDiscount());
            } catch (Exception e) {
                LOGGER.error("Error getting voucher with code: {}", request.getVoucherCode(), e);
                response.setMessage("No discount was applied due to an error.");
            }
        }
        return new ResponseEntity<>(response, OK);
    }

    @PostMapping("/calculate")
    public CompletableFuture<ResponseEntity<DeliveryCostResponse>> computeDeliveryAsync(@Valid @RequestBody ParcelRequest request) throws IOException {
        CompletableFuture<DeliveryCostResponse> deliveryFuture = deliveryService.calculateAsync(request);
        return deliveryFuture.thenCompose(response -> {
            if (shouldApplyDiscount(request)) {
                return applyVoucherDiscount(response, request);
            } else {
                return CompletableFuture.completedFuture(new ResponseEntity<>(response, OK));
            }
        });
    }

    private CompletableFuture<ResponseEntity<DeliveryCostResponse>> applyVoucherDiscount(DeliveryCostResponse response, ParcelRequest request) {
        return voucherService.callVoucherAPIAsync(request.getVoucherCode())
                .thenApply(voucher -> {
                    response.setDiscount(voucher.getDiscount());
                    return new ResponseEntity<>(response, OK);
                })
                .exceptionally(e -> {
                    handleVoucherError(response, request, e);
                    return new ResponseEntity<>(response, OK);
                });
    }

    private boolean shouldApplyDiscount(ParcelRequest request) {
        return DeliveryConfig.PARCEL_ACCEPT_DISCOUNT &&
                !request.getVoucherCode().isEmpty() &&
                request.getVoucherCode() != null;
    }

    private void handleVoucherError(DeliveryCostResponse response, ParcelRequest request, Throwable e) {
        LOGGER.error("Error getting voucher with code: {}", request.getVoucherCode(), e);
        response.setMessage("No discount was applied due to an error.");
    }
}
