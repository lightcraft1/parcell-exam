package com.parcel.delivery.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CostResult {
    private boolean calculationSucceeded;
    private DeliveryCostResponse costResponse;
}
