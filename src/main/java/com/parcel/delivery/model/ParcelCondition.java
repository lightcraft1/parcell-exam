package com.parcel.delivery.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParcelCondition {
    @JsonProperty("rule-name")
    private String ruleName;

    @JsonProperty("condition")
    private String condition;

    @JsonProperty("weight")
    private double weight;

    @JsonProperty("cost")
    private double cost;

    @JsonProperty("compute-as")
    private String computeAs;

    @JsonProperty("volume")
    private double volume;
}
