package com.parcel.delivery.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParcelRequest {

    @NotNull
    @Positive
    private double weight;

    @NotNull
    @Positive
    private double height;

    @NotNull
    @Positive
    private double width;

    @NotNull
    @Positive
    private double length;

    private String voucherCode;

    public double getVolume() {
        return this.height * this.width * this.length;
    }
}
