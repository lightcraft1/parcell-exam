package com.parcel.delivery.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;
import java.util.TreeMap;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParcelRules {
    //implemented treemap to order the priority
    @JsonProperty("priority")
    private TreeMap<Integer, String> priority;

    @JsonProperty("conditions")
    private List<ParcelCondition> conditions;
}
