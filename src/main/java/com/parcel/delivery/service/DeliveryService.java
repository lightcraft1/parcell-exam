package com.parcel.delivery.service;

import com.parcel.delivery.model.DeliveryCostResponse;
import com.parcel.delivery.model.ParcelRequest;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public interface DeliveryService {
    DeliveryCostResponse calculate(ParcelRequest request) throws IOException;
    CompletableFuture<DeliveryCostResponse> calculateAsync(ParcelRequest request) throws IOException;
}
