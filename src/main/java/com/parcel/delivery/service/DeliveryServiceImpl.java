package com.parcel.delivery.service;

import com.parcel.delivery.DeliveryConfig;
import com.parcel.delivery.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Service
public class DeliveryServiceImpl implements DeliveryService {
    //TODO: add detailed logs
    private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryServiceImpl.class);
    private static final String REJECT = "reject";
    private static final String KG = "kg";
    private final DeliveryConfig deliveryConfig;

    @Autowired
    public DeliveryServiceImpl(DeliveryConfig deliveryConfig){
        this.deliveryConfig = deliveryConfig;
    }

    @Override
    public DeliveryCostResponse calculate(ParcelRequest request) throws IOException {
        LOGGER.info("Calculating cost for request: {}", request);

        Optional<TreeMap<Integer, String>> optionalPriority = Optional.ofNullable(deliveryConfig.getParcelRules())
                .map(ParcelRules::getPriority);

        TreeMap<Integer, String> priority = optionalPriority.orElseThrow(() -> {
            String errorMessage = "Priority from ParcelRules is null";
            LOGGER.error(errorMessage);
            return new IllegalStateException(errorMessage);
        });

        CostResult costDetails = null;

        for (Map.Entry<Integer, String> entry : priority.entrySet()) {
            costDetails = this.computeCost(entry.getValue(), request);
            if (costDetails.isCalculationSucceeded()) {
                break;
            }
        }
        return costDetails.getCostResponse();
    }

    @Override
    public CompletableFuture<DeliveryCostResponse> calculateAsync(ParcelRequest request) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                LOGGER.info("Calculating cost for request: {}", request);

                Optional<TreeMap<Integer, String>> optionalPriority = Optional.ofNullable(deliveryConfig.getParcelRules())
                        .map(ParcelRules::getPriority);

                TreeMap<Integer, String> priority = optionalPriority.orElseThrow(() -> {
                    String errorMessage = "Priority from ParcelRules is null";
                    LOGGER.error(errorMessage);
                    return new IllegalStateException(errorMessage);
                });

                CostResult costDetails = null;

                for (Map.Entry<Integer, String> entry : priority.entrySet()) {
                    costDetails = this.computeCost(entry.getValue(), request);
                    if (costDetails.isCalculationSucceeded()) {
                        break;
                    }
                }
                return costDetails.getCostResponse();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }


    //used map entry to return single key value pair
    private CostResult computeCost(String ruleName, ParcelRequest request) throws IOException {
        DeliveryCostResponse costResponse = new DeliveryCostResponse();

        List<ParcelCondition> conditions = deliveryConfig.getParcelRules().getConditions();
        Optional<ParcelCondition> parcelCondition = conditions.stream()
                .filter(condition -> condition.getRuleName().equals(ruleName))
                .findFirst();

        if (parcelCondition.isPresent()) {
            ParcelCondition condition = parcelCondition.get();
            costResponse = computeCostForCondition(condition, request);

            //checking if cost has value and the or the rule name is REJECT
            if (costResponse.getCost() > 0
                    || (costResponse.getRuleName() != null
                    && costResponse.getRuleName().equalsIgnoreCase(REJECT))) {
                return new CostResult(true,costResponse);
            }
        }
        return new CostResult(false, costResponse);
    }

    private DeliveryCostResponse computeCostForCondition(ParcelCondition parcelCondition, ParcelRequest request) {
        DeliveryCostResponse costResponse = new DeliveryCostResponse();
        DeliveryConfig.ComparisonOperator operator = DeliveryConfig.ComparisonOperator.fromSymbol(parcelCondition.getCondition());

        double requestValue = KG.equalsIgnoreCase(parcelCondition.getComputeAs())
                ? request.getWeight() : request.getVolume();

        double conditionRuleValue = KG.equalsIgnoreCase(parcelCondition.getComputeAs())
                ? parcelCondition.getWeight()
                : parcelCondition.getVolume();

        if (isConditionMet(operator, requestValue, conditionRuleValue)) {
            costResponse.setCost(parcelCondition.getCost() * requestValue);
            costResponse.setRuleName(parcelCondition.getRuleName());
        }
        return costResponse;
    }

    private boolean isConditionMet(DeliveryConfig.ComparisonOperator operator, double requestValue, double conditionRuleValue) {
        switch(operator) {
            case GREATER_THAN:
                return requestValue > conditionRuleValue;
            case LESS_THAN:
                return requestValue < conditionRuleValue;
            case NONE:
                return true;
            default:
                return false;
        }
    }
}
