package com.parcel.delivery.service;

import com.parcel.delivery.model.Voucher;

import java.util.concurrent.CompletableFuture;

public interface VoucherService {
    Voucher callVoucherAPI(String voucherCode);
    CompletableFuture<Voucher> callVoucherAPIAsync(String voucherCode);
}
