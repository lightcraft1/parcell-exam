package com.parcel.delivery.service;

import com.parcel.delivery.DeliveryConfig;
import com.parcel.delivery.model.Voucher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.concurrent.CompletableFuture;

@Service
public class VoucherServiceImpl implements VoucherService {
    private static final Logger LOGGER = LoggerFactory.getLogger(VoucherServiceImpl.class);
    private final String BASE_URL = DeliveryConfig.MOCKLAB_BASE_URL;
    private final String API_KEY = DeliveryConfig.MOCKLAB_API_KEY;
    private final String VOUCHER = "voucher";

    private final RestTemplate restTemplate;

    @Autowired
    VoucherServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Voucher callVoucherAPI(String voucherCode) {
        String url = BASE_URL + "/" + VOUCHER + "/" + voucherCode;

        // set API key as a parameter
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("key", API_KEY);

        try {
            ResponseEntity<Voucher> response = restTemplate.getForEntity(uriBuilder.toUriString(), Voucher.class);
            if (response.getStatusCode().is2xxSuccessful()) {
                return response.getBody();
            }
        } catch (HttpClientErrorException e) {
            // Handle 4xx errors
            LOGGER.error("Error while calling API: {}", e.getResponseBodyAsString(), e);
        }
        return null;
    }

    @Override
    public CompletableFuture<Voucher> callVoucherAPIAsync(String voucherCode) {
        return CompletableFuture.supplyAsync(() -> {
            String url = BASE_URL + "/" + VOUCHER + "/" + voucherCode;

            // set API key as a parameter
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
                    .queryParam("key", API_KEY);

            try {
                ResponseEntity<Voucher> response = restTemplate.getForEntity(uriBuilder.toUriString(), Voucher.class);
                if (response.getStatusCode().is2xxSuccessful()) {
                    return response.getBody();
                }
            } catch (HttpClientErrorException e) {
                // Handle 4xx errors
                LOGGER.error("Error while calling API: {}", e.getResponseBodyAsString(), e);
            }
            return null;
        });
    }
}
