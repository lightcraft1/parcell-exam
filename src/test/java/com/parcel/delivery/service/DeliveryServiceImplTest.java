package com.parcel.delivery.service;

import com.parcel.delivery.DeliveryConfig;
import com.parcel.delivery.model.DeliveryCostResponse;
import com.parcel.delivery.model.ParcelCondition;
import com.parcel.delivery.model.ParcelRequest;
import com.parcel.delivery.model.ParcelRules;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class DeliveryServiceImplTest {

    @Mock
    private DeliveryConfig deliveryConfig;

    @InjectMocks
    private DeliveryServiceImpl deliveryService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCalculateReject() throws IOException {
        // Prepare data
        TreeMap<Integer, String> priority = new TreeMap<>();
        priority.put(1, "reject");
        List<ParcelCondition> conditions = new ArrayList<>();

        ParcelCondition condition = new ParcelCondition();
        condition.setRuleName("reject");
        condition.setCondition(">");
        condition.setWeight(10.0);
        condition.setCost(0.0);
        condition.setComputeAs("kg");
        conditions.add(condition);

        ParcelRules parcelRules = new ParcelRules();
        parcelRules.setPriority(priority);
        parcelRules.setConditions(conditions);

        when(deliveryConfig.getParcelRules()).thenReturn(parcelRules);

        ParcelRequest request = new ParcelRequest();
        request.setWeight(51.0);

        // Execute
        DeliveryCostResponse response = deliveryService.calculate(request);
        // Verify and Assert
         assertEquals(0.0, response.getCost());
         assertEquals("reject", response.getRuleName());
        //verify if the method is call exactly once
        verify(deliveryConfig, times(1)).getParcelRules();
    }

    @Test
    public void testCalculateHeavy() throws IOException {
        // Prepare data
        TreeMap<Integer, String> priority = new TreeMap<>();
        priority.put(1, "heavy");
        List<ParcelCondition> conditions = new ArrayList<>();

        ParcelCondition condition = new ParcelCondition();
        condition.setRuleName("heavy");
        condition.setCondition(">");
        condition.setWeight(10.0);
        condition.setCost(20.0);
        condition.setComputeAs("kg");
        conditions.add(condition);

        ParcelRules parcelRules = new ParcelRules();
        parcelRules.setPriority(priority);
        parcelRules.setConditions(conditions);

        when(deliveryConfig.getParcelRules()).thenReturn(parcelRules);

        ParcelRequest request = new ParcelRequest();
        request.setWeight(11.0);

        // Execute
        DeliveryCostResponse response = deliveryService.calculate(request);
        // Verify and Assert
        assertEquals(220, response.getCost());
    }

    //TODO: small
    //TODO: medium
    //TODO: large
    //TODO: rule priority
    //TODO: compute cost
    //TODO: computeCostForCondition

}